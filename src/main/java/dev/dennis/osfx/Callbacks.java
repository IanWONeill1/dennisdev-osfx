package dev.dennis.osfx;

public interface Callbacks {
    void onFrameStart();

    void onFrameEnd();
}
