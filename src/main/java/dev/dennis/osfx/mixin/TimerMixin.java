package dev.dennis.osfx.mixin;

import dev.dennis.mixin.Mixin;
import dev.dennis.osfx.api.Timer;

@Mixin("Timer")
public abstract class TimerMixin implements Timer {
}
