package dev.dennis.osfx.mixin;

import dev.dennis.mixin.Mixin;
import dev.dennis.osfx.api.Model;

@Mixin("Model")
public abstract class ModelMixin implements Model {
}
